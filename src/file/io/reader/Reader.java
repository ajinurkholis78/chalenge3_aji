package file.io.reader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Reader {
    public List<String> kelas = new LinkedList<>();
    public Map<String, List<Integer>> csv(String url){

        Map<String, List<Integer>> tampungData = new TreeMap<>();
        String line ="";
        try {
            File file = new File(url);
            FileReader fileReader = new FileReader(file);
            BufferedReader br = new BufferedReader(fileReader);
            while ((line = br.readLine()) != null){
                String[] values = line.split(";");
                kelas.add(values[0]);
                List<Integer> tamp = new LinkedList<>();
                for (int i = 1; i < values.length; i++) {
                    tamp.add(Integer.parseInt(values[i]));
                }
                tampungData.put(values[0], tamp);
            }
            br.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return tampungData;
    }
}
