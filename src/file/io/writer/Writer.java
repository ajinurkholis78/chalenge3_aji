package file.io.writer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Writer{

    public static void modusMeanMedianTxt(List<String> kelas, Map<String, Integer> modus, Map<String, Double> median, Map<String, Double> mean, String url){
        try {
            File file = new File(url);
            if (file.createNewFile()){
                sucsess(url);
            }else {
                failed();
            }
            FileWriter fw = new FileWriter(file);
            BufferedWriter bwr = new BufferedWriter(fw);
            bwr.write("Berikut Hasil Pengolahan Nilai");
            bwr.newLine();
            bwr.newLine();
            bwr.write( "Berikut hasil sebaran data nilai\n");
            bwr.write("----------------------------------------\n");
            for (int i = 0; i < kelas.size(); i++) {
                bwr.write(kelas.get(i));
                bwr.newLine();
                bwr.write("Mean   : " + String.format("%.2f",mean.get(kelas.get(i))));
                bwr.newLine();
                bwr.write("Median : " + median.get(kelas.get(i)).toString());
                bwr.newLine();
                bwr.write("Modus  : " + modus.get(kelas.get(i)).toString());
                bwr.newLine();
                bwr.newLine();
            }
            bwr.newLine();
            bwr.flush();
            bwr.close();

        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public static void modusFrequenci(List<String> kelas, List<Map<Integer, Integer>> modusFrequenci, String url){
        try {
            File file = new File(url);
            if (file.createNewFile()){
                sucsess(url);
            }else {
                failed();
            }
            FileWriter fw = new FileWriter(file);
            BufferedWriter bwr = new BufferedWriter(fw);

            bwr.write("Berikut Hasil Pengolahan Nilai");
            bwr.newLine();
            bwr.newLine();
            for (int i = 0; i < kelas.size(); i++) {
                bwr.write(kelas.get(i) + " : \n");
                bwr.write("-----------------------------\n");
                bwr.write( "|Nilai           | Frekuensi|\n");
                bwr.write("-----------------------------\n");
                Map<Integer,Integer> temp = modusFrequenci.get(i);
                Set<Map.Entry<Integer, Integer>> entries = temp.entrySet();
                int lessThanSix = 0;
                for (var entry:
                     entries) {
                    if (entry.getKey() < 6 ){
                        lessThanSix += entry.getValue();
                    }
                }
                bwr.write(String.format("|Kurang dari 6   | %-9d|",lessThanSix));
                bwr.newLine();
                for (var entry:
                     entries) {
                    if (entry.getKey() >= 6) {
                        bwr.write(String.format("|%-15d | %-9d|\n", entry.getKey(), entry.getValue()));
                    }
                }
                bwr.write("-----------------------------");
                bwr.newLine();
                bwr.newLine();
            }
            bwr.flush();
            bwr.close();

        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public static void sucsess(String url){
        System.out.println("File telah di generate di " + url);
        System.out.println("Silahkan Dicek");
        System.out.println();
    }

    public static void failed(){
        System.out.println("File tidak ditemukan/gagal dibuat");
        System.out.println();
    }
}