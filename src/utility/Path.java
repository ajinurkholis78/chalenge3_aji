package utility;

public class Path {
    private final String pathReaderCsv = "C://temp//direktori/data_sekolah.csv";
    private final String pathWriterModusTxt = "C://temp//direktori/data_sekolah_modus.txt";
    private final String pathWriterModusMedianMeanTxt = "C://temp//direktori/data_sekolah_modus_median_mean.txt";
    private final String pathWriterModusCsv = "C://temp//direktori/data_sekolah_modus.csv";
    private final String pathWriterModusMedianMeanCsv = "C://temp//direktori/data_sekolah_modus_median_mean.csv";

    public String getPathReaderCsv() {
        return pathReaderCsv;
    }

    public String getPathWriterModusTxt() {
        return pathWriterModusTxt;
    }

    public String getPathWriterModusMedianMeanTxt() {
        return pathWriterModusMedianMeanTxt;
    }

    public String getPathWriterModusCsv() {
        return pathWriterModusCsv;
    }

    public String getPathWriterModusMedianMeanCsv() {
        return pathWriterModusMedianMeanCsv;
    }
}
