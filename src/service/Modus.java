package service;

import java.util.*;

public class Modus {
    public Map<String, Integer> modus(Map<String, List<Integer>> data) {
        Map<String, Integer> modus = new HashMap<>();
        Set<Map.Entry<String, List<Integer>>> entries = data.entrySet();
        int result = 0;
        int h = 0;
        int j = 0;
        int max = 0;
        for (var entry: entries) {
            List<Integer> hasil = new ArrayList<>();
            double temp=0;
            for (var value: data.get(entry.getKey())) {
                hasil.add(value);
            }
            int[] dataArray = new int[hasil.size()];
            for (int i = 0; i < hasil.size(); i++) {
                dataArray[i] = hasil.get(i);
            }
            for (int i:
                    dataArray) {
                if (i == j){
                    h++;
                    if (h>max){
                        max = h;
                        result = j;
                    }
                }else {
                    j = i;
                    h = 1;
                }
            }
            modus.put(entry.getKey(), result);
        }
        return modus;
    }

    public List<Map<Integer, Integer>> modusFrekuensi(Map<String, List<Integer>> data) {
        List<Map<Integer, Integer>> modus = new ArrayList<>();
        Set<Map.Entry<String, List<Integer>>> entries = data.entrySet();
        for (var entry: entries) {
            HashMap<Integer, Integer> frequency = new HashMap<>();
            List<Integer> hasil = new ArrayList<>();
            for (var value: data.get(entry.getKey())) {
                hasil.add(value);
            }
            int[] dataArray = new int[hasil.size()];
            for (int i = 0; i < hasil.size(); i++) {
                dataArray[i] = hasil.get(i);
            }
            for(int i=0; i< dataArray.length;i++){
                Integer ch = dataArray[i];
                Integer value=frequency.get(ch);
                if(value!=null){
                    frequency.put(ch, value + 1);
                }else{
                    frequency.put(ch, 1);
                }
            }
            modus.add(frequency);
        }
        return modus;
    }
}
