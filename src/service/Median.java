package service;

import java.util.*;

public class Median {
    public Map<String, Double> median(Map<String, List<Integer>> data) {
        Map<String, Double> median = new HashMap<>();
        Set<Map.Entry<String, List<Integer>>> entries = data.entrySet();
        for (var entry: entries) {
            List<Double> hasil = new ArrayList<>();
            double temp=0;
            for (var value: data.get(entry.getKey())) {
                hasil.add(Double.valueOf(value));
            }
            double[] dataArray = new double[hasil.size()];
            for (int j = 0; j < hasil.size(); j++) {
                dataArray[j] = hasil.get(j);
            }
            if (dataArray.length % 2 == 1) {
                temp = dataArray[dataArray.length / 2];
             } else {
                temp = ((dataArray[dataArray.length / 2] + dataArray[(dataArray.length / 2) - 1])) / 2;
            }
            median.put(entry.getKey(), temp);
        }
        return median;
    }
}
