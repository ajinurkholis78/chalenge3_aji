package service;

import java.util.*;

public class Mean{
    public Map<String, Double> mean(Map<String, List<Integer>> data) {
        Map<String, Double> mean = new TreeMap<>();
        Set<Map.Entry<String, List<Integer>>> entries = data.entrySet();
        for (var entry: entries) {
            List<Double> hasil = new ArrayList<>();
            double temp=0;
            for (var value: data.get(entry.getKey())) {
                hasil.add(Double.valueOf(value));
            }
            double[] dataArray = new double[hasil.size()];
            for (int j = 0; j < hasil.size(); j++) {
                dataArray[j] = hasil.get(j);
                temp += dataArray[j];
            }
            mean.put(entry.getKey(), temp/dataArray.length);
        }
        return mean;
    }
}
