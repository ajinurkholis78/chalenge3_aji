package menu;
import java.util.Scanner;

public class Menu {

    private int userInputMainMenu, userInputLastMenu;
    public Scanner sc = new Scanner(System.in);

    public int getUserInputMainMenu() {
        return userInputMainMenu;
    }

    public void setUserInputMainMenu(int userInputMainMenu) {
        this.userInputMainMenu = userInputMainMenu;
    }

    public int getUserInputLastMenu() {
        return userInputLastMenu;
    }

    public void setUserInputLastMenu(int userInputLastMenu) {
        this.userInputLastMenu = userInputLastMenu;
    }

    public void mainMenu(){
        header();
        System.out.println("1. Generate txt untuk menampilkan modus");
        System.out.println("2. Generate txt untuk menampilkan modus, mean, median");
        System.out.println("3. Generate txt kedua File");
        System.out.println("0. Exit");
        System.out.print("Pilih Menu : ");
    }

    public void header(){
        System.out.println("""
            ------------------------------------------------
            >>>>    Aplikasi Pengolah Nilai Siswa       <<<<
            ------------------------------------------------
            """);
    }

    public void lastMenu(){
        System.out.println("0. Exit");
        System.out.println("1. Kembali Kemenu utama");
        System.out.print("Pilih Menu : ");
    }
}
