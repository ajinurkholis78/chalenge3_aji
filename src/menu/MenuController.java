package menu;

import file.io.reader.Reader;
import file.io.writer.Writer;
import service.Mean;
import service.Median;
import service.Modus;
import utility.Path;
import java.util.*;

public class MenuController {

    private static boolean kondisi = true;
    public static void onStart(){
        Reader reader = new Reader();
        Writer writer = new Writer();
        Menu menu = new Menu();
        Mean mean = new Mean();
        Median median = new Median();
        Modus modus = new Modus();
        Path path = new Path();
        Map<String, List<Integer>> data = new TreeMap<>(
                reader.csv(
                        path.getPathReaderCsv()));
        Set<Map.Entry<String, List<Integer>>> entries = data.entrySet();
        for (var value: entries) {
            Collections.sort(value.getValue());
        }
        while (kondisi) {
            menu.mainMenu();
            menu.setUserInputMainMenu(menu.sc.nextInt());
            switch (menu.getUserInputMainMenu()) {
                case 0 -> {
                    footer();
                }
                case 1 -> {
                    menu.header();
                    writer.modusFrequenci(reader.kelas,
                            modus.modusFrekuensi(data),
                            path.getPathWriterModusTxt());

                    menu.lastMenu();
                    menu.setUserInputLastMenu(menu.sc.nextInt());
                    if (menu.getUserInputLastMenu() == 0) {
                        footer();
                    }
                }
                case 2 -> {
                    menu.header();
                    writer.modusMeanMedianTxt(reader.kelas,
                            modus.modus(data), median.median(data),
                            mean.mean(data),
                            path.getPathWriterModusMedianMeanTxt());

                    menu.lastMenu();
                    menu.setUserInputLastMenu(menu.sc.nextInt());
                    if (menu.getUserInputLastMenu() == 0) {
                        footer();
                    }
                }
                case 3 -> {
                    menu.header();
                    writer.modusFrequenci(reader.kelas,
                            modus.modusFrekuensi(data),
                            path.getPathWriterModusTxt());

                    writer.modusMeanMedianTxt(reader.kelas,
                            modus.modus(data),
                            median.median(data),
                            mean.mean(data),
                            path.getPathWriterModusMedianMeanTxt());

                    menu.lastMenu();
                    menu.setUserInputLastMenu(menu.sc.nextInt());
                    if (menu.getUserInputLastMenu() == 0) {
                        footer();
                    }
                }
                default -> {
                    System.out.println("Salah Input");
                    menu.mainMenu();
                }
            }
        }
    }

    public static void footer(){
        System.out.println("""
                        
                        <><><><><><><><><><><><><><><><><><><><><><><><><><><><><>
                                Terima Kasih Telah Menggunakan Aplikasi Ini 
                                           -Aji Nurkholis-
                        """);
        kondisi = false;
        System.exit(0);
    }
}